import React, { Component } from 'react';
import header from './assets/images/header.png';
import {
	Row,
	Col,
	Jumbotron,
	Container
} from 'reactstrap';
import Subscriber from './components/Subscriber/Subscriber';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
	      <Jumbotron className="header" fluid>
		      <Container fluid>
			      <p className="lead">
				      <span>
					      “Marry the one who gives you the same feeling you get when you see your food coming at a restaurant.”
				      </span>
			      </p>
		      </Container>
	      </Jumbotron>
	      <Subscriber/>
	      <Jumbotron className="footer" fluid>
		      <Container fluid>
			      <p className="lead">
				      <span>
					      “Happiness is planning a trip to somewhere new with someone you love.”
				      </span>
			      </p>
		      </Container>
	      </Jumbotron>
      </div>
    );
  }
}

export default App;
