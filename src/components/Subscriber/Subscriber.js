import React, { Component } from 'react';
import Gallery from "react-photo-gallery";
import {
	Row,
	Col,
	Container,
} from 'reactstrap';

import map from '../../assets/images/map.png';
import mapParking from '../../assets/images/wegplan-trouw.jpg';
import dresscode1 from '../../assets/images/dresscode1.jpg';
import dresscode2 from '../../assets/images/dresscode2.jpg';
import dresscode3 from '../../assets/images/dresscode3.jpg';
import dresscode4 from '../../assets/images/dresscode4.jpg';
import dresscode5 from '../../assets/images/dresscode5.jpg';
import dresscode6 from '../../assets/images/dresscode6.jpg';
import dresscode7 from '../../assets/images/dresscode7.jpg';
import dresscode8 from '../../assets/images/dresscode8.jpg';


import {weddingCodes} from './WeddingCodes';
import Intro from './Intro';
import Program from './Program';
import Overview from './Overview';
import CheckInField from './CheckInField';

const photos = [
	{
		src: dresscode1,
		width: 4,
		height: 3
	},
	{
		src: dresscode2,
		width: 1,
		height: 1
	},
	{
		src: dresscode3,
		width: 3,
		height: 4
	},
	{
		src: dresscode4,
		width: 3,
		height: 4
	},
	{
		src: dresscode7,
		width: 4,
		height: 5
	},
	{
		src: dresscode8,
		width: 4,
		height: 5
	},
	{
		src: dresscode5,
		width: 7,
		height:7
	},
	{
		src: dresscode6,
		width: 8,
		height: 7
	}
];

class Subscriber extends Component {
	constructor (props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.handleChangeSelectProgram = this.handleChangeSelectProgram.bind(this);
		this.state = {
			weddingCode: '',
			formStep: 1,
			names:[],
			form: {},
			allowedEvents: []
		}
	}

	handleChange(event) {
		let input = event.target.value;
		let weddingCode = weddingCodes.find(obj => obj.code === input);
		const allowedEvents = weddingCode ? weddingCode.allowedEvents : [];
		const formStep = weddingCode ? 2 : 1;
		this.setState({
			weddingCode: input,
			allowedEvents: allowedEvents,
			formStep: formStep
		})
	}

	handleChangeSelectProgram(form, names) {
		this.setState({
			form: form,
			names: names,
			formStep: 3
		})
	}

	render() {
		const {formStep, input, allowedEvents, form, names} =  this.state;
		return (
			<React.Fragment>
				{formStep === 1 &&
					<div className="pt-5 pb-5 bg-primary-color">
						<Col>
							<Container>
								<Row>
									<Col sm="12">
										<p>Hoi, het is bijna zover. Wil je nog even iets nakijken, vul dan je gate nummer in.</p>
									</Col>
								</Row>
							</Container>
						</Col>
					</div>
				}
				<div className="pt-5 pb-5">
					<Col sm="12">
						<Container>
							{(() => {
								switch(formStep) {
									case 1:
										return <CheckInField
												   input={input}
								                   allowedEvents={allowedEvents}
								                   onChange={this.handleChange} />;
									/*case 2:
										return <Program onSubmit={this.handleChangeSelectProgram} allowedEvents={allowedEvents}  />;*/
									case 2:
										return <Overview chosenEvents={allowedEvents} form={form} names={names}  />;
								}
							})()}
						</Container>
					</Col>
				</div>
				{formStep === 2 &&
					<React.Fragment>
						<div className="pt-5 pb-5 bg-primary-color">
							<Col sm="12">
								<Container>
									<h4>Wegbeschrijving</h4>
									<p>Het Gemeentepark van Brasschaat rijdt je in via Hemelakkers. <br/>
									Volg de gele weg naar de parking vlakbij de Ruiterhal. <br/>
									Van hieruit bereik je makkelijk The Mansion en het Boothuisje.</p>
									<a target="_blank" href="https://www.google.com/maps/place/Gemeentepark,+2930+Brasschaat/data=!4m2!3m1!1s0x47c407c4a85e0b2b:0xee92648d43a53918?sa=X&ved=2ahUKEwiGiLrtjK_hAhVxxoUKHQ0FAxUQ8gEwAHoECAoQAQ">
										<img src={map} style={{'width': '50%'}} alt="map" />
									</a>
									<img src={mapParking} style={{'width': '100%'}} alt="map-trouw" />
								</Container>
							</Col>
						</div>

						<div className="pt-5 pb-5">
							<Col sm="12">
								<Container>
									<h4>Dresscode</h4>
									<p><b>Colorful chique</b><br/>
										27 juli wordt een kleurrijke dag.<br/>
										Zoek je nog een geschikte outfit,<br/>
										hou dan vooral de woorden 'kleurrijk' en 'klassevol' in je achterhoofd<br/>
										én elimineer zwart volledig !<br/>
										Een elegante hak of comfy plat,<br/>
										Lang of kort,<br/>
										In full color of licht zomers met een kleurrijk accent...</p>

										<p>Veel plezier met het uitzoeken van jouw outfit !</p>

										<p>PS : Een grote glimlach en dansbenen zijn verplicht.</p>
								</Container>
							</Col>
							<div id="flexbox-grid">
								<Gallery photos={photos} direction={"column"} />
							</div>
						</div>

					</React.Fragment>
				}
			</React.Fragment>
		);
	}
}

export default Subscriber;
