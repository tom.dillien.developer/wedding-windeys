import React, { Component } from 'react';
import * as emailJs from "emailjs-com";
import { renderEmail } from 'react-html-email';
class Overview extends Component {
	componentDidMount() {
		window.scrollTo(0, 0);
	}

	sendMail = () => {
		const templateParams = {
			reply_email: 'amort@myglockneronline.com',
			email_html: renderEmail(this.renderProgram())
		};

		if (this.props.form.email.length >= 1) {
			templateParams.reply_email = this.props.form.email
		}

		if (this.props.form.comment.length >= 1) {
			templateParams.message = this.props.form.comment
		}

		emailJs.send('default_service', 'http_wedding_ny2707_be', templateParams, 'user_1Lgs9pF0Od2jv3WUCgEBg')
			.then(function(response) {
				console.log('SUCCESS!', response.status, response.text);
			}, function(error) {
				console.log('FAILED...', error);
			});
	}

	renderNames= () => {
		return (<div>
			<h4>wij verwachten jullie: </h4>
			<ul>
				{this.props.names.map((person, index) => (
					<li>{person.name}</li>
				))}
			</ul>
		</div>);
	}

	renderProgram = () => {
		const {chosenEvents} = this.props;
		if (this.props.form.checkOut) {
			return(
				<div>
					<h4>
						Je bent uitgeschreven voor de trouw van Nick en Yana
					</h4>
				</div>
			)
		}

		return (
			<div>
				<h4>Programma van zaterdag 27 juli 2019</h4>
				{chosenEvents.includes("cityHall") &&
				<p><i>11u15 - 15u00 Gemeentehuis & Lunch</i><br/>
					@ de Robianostraat 64, 2150 Borsbeek<br/>
					@ The Mansion of Dragons, 2930 Brasschaat <br/>
					De officiële dienst begint stipt om 11u30.<br/>
					Parkeren kan in de Robianostraat, op de parking tegenover Keurslager Wydooghe (t.h.v. huisnummer 14) of op de parking van Ter Smisse in de Louis van Regenmortellei. Vergeet je parkeerschijf niet.</p>
				}

				{chosenEvents.includes("ceremony") &&
				<p><i>15u00 – 18u00 Ceremonie & Receptie</i><br/>
					@ Het Boothuisje, Gemeentepark 21, 2930 Brasschaat<br/>
					De ceremonie start stipt op 15u30.</p>
				}

				{chosenEvents.includes("dinner") &&
				<p><i>18u00 – 21u00 Diner</i><br/>
					@ The Mansion of Dragons, Gemeentepark 12, 2930 Brasschaat<br/>
					(no kids allowed)</p>
				}

				{chosenEvents.includes("afterparty") &&
				<p><i>21u30 - 03u00 Party & Dessert</i><br/>
					@ The Mansion of Dragons, Gemeentepark 12, 2930 Brasschaat<br/>
					(no kids allowed)</p>
				}
				<div>
				{/*	{this.renderNames()}*/}
				</div>
			</div>
		);
	}

	render() {
		//this.sendMail();
		return this.renderProgram();
	}
}

export default Overview;
