import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import {
	Row,
	Col,
	Form,
	FormGroup,
	Label,
	Input,
	Button,
	ListGroup,
	ListGroupItem,
	CustomInput,
	FormFeedback} from 'reactstrap';

class Overview extends Component {
	constructor (props) {
		super(props);
		this.state = {
			names: [{ name: "" }],
			form: {
				chosenEvents: props.allowedEvents,
				allowedEvents: props.allowedEvents,
				email: '',
				name: '',
				comment: '',
				errors: {
					name: false
				}
			},
		}
	}

	componentDidMount() {
		window.scrollTo(0, 0);
	}

	handleCheckboxChange(event) {
		const isChecked = event.target.checked;
		const name = event.target.name;
		const allowedEvents = this.state.form.allowedEvents;

		if (allowedEvents.includes(name)) {
			const chosenEvents = this.state.form.chosenEvents;
			const updatedChosenEvents = isChecked ? chosenEvents.concat(name) : chosenEvents.filter(item => item !== name);
			this.setState(
				Object.assign(this.state.form,{chosenEvents: updatedChosenEvents})
			);
			this.setState({
				form: {
					...this.state.form,
					checkOut: false
				}
			});
		}
	}

	handleCheckout(event) {
		this.setState({
			form: {
				...this.state.form,
				checkOut: event.target.checked,
				chosenEvents: []
			}
		});
	}

	handleShareholderNameChange = idx => evt => {
		const newShareholders = this.state.names.map((shareholder, sidx) => {
			if (idx !== sidx) return shareholder;
			return { ...shareholder, name: evt.target.value };
		});

		this.setState({ names: newShareholders });
	};


	handleAddShareholder = () => {
		this.setState({
			names: this.state.names.concat([{ name: "" }])
		});
	};

	handleRemoveShareholder = idx => () => {
		if (this.state.names.length !== 1) {
			this.setState({
				names: this.state.names.filter((s, sidx) => idx !== sidx)
			});
		}
	}

	handleInputChange(event) {
		this.setState(Object.assign(this.state.form, {[event.target.name]: event.target.value}));
	}

	handleSubmit(event) {
		const validation = this.validate(
			this.state.names
		);

		// Check if every field in validation errors is false or not
		if(_.every(_.values(validation), function(v) {return !v;})) {
			// send email and hide form
			this.props.onSubmit(this.state.form, this.state.names);
		} else {
			this.setState({
				form: {
					...this.state.form,
					errors:validation
				}
			})
		}
	}

	 validate(names) {
		const response = names.map(function (element) {
			return element.name.length < 1;
		 });

		return response;
	}

	render() {
		const {chosenEvents, allowedEvents, email, comment, checkOut, errors } = this.state.form;
		if (allowedEvents.length === 0) { return null; }

		return (
			<div>
				<Form>
					<FormGroup>
						<h4>Jouw programma van zaterdag 27 juli 2019</h4>
							<ListGroup className="mt-3">
								{allowedEvents.includes("cityHall")  &&
									<ListGroupItem>
										<Row>
											<Col sm="9">
												<span className="float-left">11u15 - 15u00 Gemeentehuis & Lunch</span><br/>
												<span className="text-muted">@ De Robianostraat 64, 2150 Borsbeek</span><br/>
												<span className="text-muted">@ The Mansion of Dragons, 2930 Brasschaat</span>
											</Col>
											<Col sm="3">
												<CustomInput type="checkbox" id="cityHall" label="check in" name="cityHall" checked={chosenEvents.includes("cityHall")} onChange={this.handleCheckboxChange.bind(this)}/>
											</Col>
										</Row>
									</ListGroupItem>
								}
								{allowedEvents.includes("ceremony") &&
									<ListGroupItem>
										<Row>
											<Col sm="9">
												<span className="float-left">15u00 – 18u00 Ceremonie & Receptie</span><br/>
												<span className="text-muted">@ Het Boothuisje, Gemeentepark 21, 2930 Brasschaat</span>
											</Col>
											<Col sm="3">
												<CustomInput type="checkbox" id="ceremony" name="ceremony"
												             label="check in"
												             checked={chosenEvents.includes("ceremony")}
												             onChange={this.handleCheckboxChange.bind(this)}/>
											</Col>
										</Row>
									</ListGroupItem>
								}
								{allowedEvents.includes("dinner") &&
									<ListGroupItem>
										<Row>
											<Col sm="9">
												<span className="float-left">18u00 – 21u00 Diner</span><br/>
												<span className="text-muted">@ The Mansion of Dragons, Gemeentepark 12, 2930 Brasschaat</span>
											</Col>
											<Col sm="3">
												<CustomInput type="checkbox" id="dinner" label="check in" name="dinner"
												             checked={chosenEvents.includes("dinner")}
												             onChange={this.handleCheckboxChange.bind(this)}/>
											</Col>
										</Row>
									</ListGroupItem>
								}
								{allowedEvents.includes("afterparty") &&
									<ListGroupItem>
										<Row>
											<Col sm="9">
												<span className="float-left">21u30 - 03u00 Party & Dessert</span><br/>
												<span className="text-muted">@ The Mansion of Dragons, Gemeentepark 12, 2930 Brasschaat</span>
											</Col>
											<Col sm="3">
												<CustomInput type="checkbox" id="afterparty" label="check in"
												             name="afterparty" checked={chosenEvents.includes("afterparty")}
												             onChange={this.handleCheckboxChange.bind(this)}/>
											</Col>
										</Row>
									</ListGroupItem>
								}
							<ListGroupItem>
								<Row>
									<Col sm="9">
										<span className="float-left">Ik zal er niet bij zijn</span><br/>
									</Col>
									<Col sm="3">
										<CustomInput type="checkbox" id="checkOut" label="check out" checked={checkOut} onChange={this.handleCheckout.bind(this)} name="checkOut" />
									</Col>
								</Row>
							</ListGroupItem>
						</ListGroup>
						<div>
						</div>
					</FormGroup>
					<Row form>
						<Col md={6}>
							<fieldset>
								<FormGroup as={Row} className={"person-wrapper"}>
									<div id="persons">
										{this.state.names.map((person, index) => {
											return (
												<Row>
													<Col>
														<Label for="name">Persoon {index + 1}: Voor- en achternaam</Label>
														<Input type="text" name="name" id="name" value={person.name}
														       onChange={this.handleShareholderNameChange(index) }
														       invalid={errors[index]} required/>
														<FormFeedback>Voornaam- en achternaam zijn verplicht</FormFeedback>
													</Col>
													<Col>
														<Button
															type="button"
															onClick={this.handleRemoveShareholder(index)}
															className="small"
														>Verwijder persoon</Button>
													</Col>
												</Row>
											)
										})}
									</div>
									<Button
										type="button"
										onClick={this.handleAddShareholder}
										className="small mt-2"
									>
										Voeg nog iemand toe
									</Button>
								</FormGroup>
							</fieldset>
						</Col>
					</Row>
					<Row form>
						<Col md={6} className="mt-3">
							<FormGroup >
								<Label>jouw mailadres</Label>
								<Input type="email"  name="email" id="email"  value={email} onChange={this.handleInputChange.bind(this)}  />
							</FormGroup>
							<FormGroup>
								<Label for="comments">Wil je ons nog een boodschap meegeven?</Label>
								<Input type="textarea" name="comment" id="comments" value={comment} onChange={this.handleInputChange.bind(this)}  />
							</FormGroup>
						</Col>
					</Row>
					<Button onClick={this.handleSubmit.bind(this)}>Check-in bevestigen</Button>
				</Form>
			</div>
		);
	}
}

Overview.propTypes = {
	allowedEvents: PropTypes.array
}

Overview.defaultProps = {
	allowedEvents: []
}

export default Overview;
