import React, { Component } from 'react';
import {
	Row,
	Col} from 'reactstrap';

class Intro extends Component {
	render() {
		const formStep = this.props.formStep;
		return (
			<Row>
				{(() => {
					switch(formStep) {
						case 1:
							return <Col sm="12">
								<p>Hoi, het is bijna zover. Wil je nog even iets nakijken, vul dan je gate nummer in.</p>
							</Col>;
						case 3:
							return <Col sm="12">
								<p>Wij hebben jou er op 27 juli heel graag bij,<br/>
								daarom vinkten we alvast de dagdelen aan waar wij een plaatsje voor jou voorzien.<br/>
								Lukt een bepaald moment op onze grote dag toch niet,<br/>
								dan verwijder je het vinkje bij dat dagdeel.<br/>
								Ben je de ganse dag verhinderd,<br/>
								dan zet je enkel een vinkje bij ‘ik kan er niet bij zijn'.</p>

								<p>Gelieve je vòòr 30 juni via onderstaande agenda in te checken.
									Wij hopen alvast dat je erbij zal zijn!</p>
								<p>PS: De kindjes zijn welkom tot en met de Ceremonie & Receptie. Dinner & Party zijn zonder kids.</p>
							</Col>;
					}
				})()}
			</Row>
		);
	}
}

export default Intro;
