export const weddingCodes  = [
	{
		code: 'NY2707',
		allowedEvents: [
			'cityHall',
			'ceremony',
			'dinner',
			'afterparty'
		]
	},
	{
		code: 'NY2019',
		allowedEvents: [
			'ceremony',
			'dinner',
			'afterparty'
		]
	},
	{
		code: 'NY2008',
		allowedEvents: [
			'ceremony',
			'afterparty'
		]
	}
];

export default {
	weddingCodes,
}