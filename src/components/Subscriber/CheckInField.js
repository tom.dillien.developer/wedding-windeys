import React, { Component } from 'react';
import {
	Form,
	FormGroup,
	Label,
	Input,
	FormFeedback,
} from 'reactstrap';


class CheckInField extends Component {

	handleChange(event) {
		this.props.onChange(event);
	}

	render() {
		const {
			input,
			allowedEvents
		} = this.props;

		return (
			<Form>
				<FormGroup>
					<Label for="exampleEmail"><h4>Gate nummer:
					</h4></Label>
					<Input
						value={input}
						onChange={this.handleChange.bind(this)}
						invalid={!allowedEvents && input !== ""}
						className="form-control form-control-lg"
						type="text"
						name="weddingCode"
						id="weddingCode"
						placeholder="#AF2000" />
					<FormFeedback>Geen geldige code</FormFeedback>
				</FormGroup>
			</Form>
		);
	}
}

export default CheckInField;
